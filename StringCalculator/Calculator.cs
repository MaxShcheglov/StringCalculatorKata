﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace StringCalculator
{
	public class Calculator
	{
		private DelimiterManager delimiterManager = new DelimiterManager();
		private NumericParser numericParser = new NumericParser();
		private NumericAdder numericAdder = new NumericAdder(); 

		private int result;

		public int Add(string numbers)
		{
			result = 0;

			if (!IsEmptyNumbersStringPassed(numbers))
			{
				delimiterManager.ExtractDifferentDelimiterIfExists(ref numbers);

				var parsedNumbers = numericParser.Parse(numbers, delimiterManager.Delimiters);

				var negativeNumbers = parsedNumbers.Where((i) => (i < 0));

				if (negativeNumbers.Count() > 0)
					ThrowExceptionWithMessage(negativeNumbers);

				result = numericAdder.SumWithoutNumbersBiggerThatUpperLimit(parsedNumbers, 1001);
			}

			return result;
		}

		private bool IsEmptyNumbersStringPassed(string numbers) => string.IsNullOrEmpty(numbers);

		private void ThrowExceptionWithMessage(IEnumerable<int> negatives)
		{
			throw new Exception("negatives not allowed: " + string.Join(",", negatives));
		}
	}

	public class DelimiterManager
	{
		private const string startDelimiterSeparator = "//";
		private const string endDelimiterSeparator = "\n";
		private const string startMultisymbolDelimiterSign = "[";
		private const string endMultisymbolDelimiterSign = "]";

		public string[] Delimiters { get; private set; }

		public DelimiterManager()
		{
			Delimiters = new string[] { ",", "\n" };
		}

		public bool IsDifferentDelimiterExists(string numbers) =>
					numbers.Contains(startDelimiterSeparator) &&
					numbers.Contains(endDelimiterSeparator);

		public void ExtractDifferentDelimiterIfExists(ref string numbers)
		{
			if (!IsDifferentDelimiterExists(numbers))
				return;

			var stringWithDelimiters = numbers.Substring(
				numbers.IndexOf(startDelimiterSeparator) +
				startDelimiterSeparator.Length,
				numbers.IndexOf(endDelimiterSeparator) - startDelimiterSeparator.Length
			);

			var multisymbolDelimiterExists = stringWithDelimiters.Contains(startMultisymbolDelimiterSign) ||
											  stringWithDelimiters.Contains(endMultisymbolDelimiterSign);

			if (multisymbolDelimiterExists)
			{
				stringWithDelimiters = stringWithDelimiters.Substring(1);
				stringWithDelimiters = stringWithDelimiters.Substring(0, stringWithDelimiters.Length - 1);
			}

			Delimiters = stringWithDelimiters.Split(
				new string[] { endMultisymbolDelimiterSign + startMultisymbolDelimiterSign }, 
				StringSplitOptions.None);

			numbers = numbers.Substring(numbers.IndexOf(endDelimiterSeparator));
		}
	}

	public class NumericParser
	{
		public IEnumerable<int> Parse(string numbers, string[] delimiters)
		{
			return numbers.Split(delimiters, StringSplitOptions.None).Select(int.Parse);
		}
	}

	public class NumericAdder
	{
		public int SumWithoutNumbersBiggerThatUpperLimit(IEnumerable<int> numbers, int upperLimit)
		{
			return numbers.Where((i) => (i < upperLimit)).Sum();
		}
	}
}
