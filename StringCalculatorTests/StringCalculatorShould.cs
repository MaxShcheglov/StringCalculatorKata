﻿using System;
using NUnit.Framework;
using StringCalculator;

namespace StringCalculatorTests
{
	[TestFixture]
	public class StringCalculatorShould
    {
		[Test]
		public void ReturnZeroIfEmptyStringPassed()
		{
			var calc = new Calculator();

			var result = calc.Add("");

			Assert.That(0, Is.EqualTo(result));
		}

		[Test]
		public void ReturnSumIfOneNumberPassed()
		{
			var calc = new Calculator();

			var result = calc.Add("1");

			Assert.That(1, Is.EqualTo(result));
		}

		[Test]
		public void ReturnSumIfTwoNumbersPassed()
		{
			var calc = new Calculator();

			var result = calc.Add("1,2");

			Assert.That(3, Is.EqualTo(result));
		}

		[Test]
		public void ReturnSumIfSomeNumberOfNumbersPassed()
		{
			var calc = new Calculator();

			var result = calc.Add("1,2,4");

			Assert.That(7, Is.EqualTo(result));
		}

		[Test]
		public void ReturnSumIfPassedNewLineBetweenNumbersInsteadOfCommas()
		{
			var calc = new Calculator();

			var result = calc.Add("1\n2,3");

			Assert.That(6, Is.EqualTo(result));
		}

		[Test]
		public void ReturnSumIfPassedNumbersWithDifferentDelimiter()
		{
			var calc = new Calculator();

			var result  = calc.Add("//;\n1;2");

			Assert.That(3, Is.EqualTo(result));
		}
		
		[Test]
		public void ThrowExceptionIfPassedNumbersWithNegatives()
		{
			var calc = new Calculator();

			Assert.Throws<Exception>(new TestDelegate(() => calc.Add("-1,1,2")));
		}

		[Test]
		public void ThrowExceptionWithMessageIfPassedNumbersWithNegatives()
		{
			var calc = new Calculator();

			Assert.Throws(Is.TypeOf<Exception>().And.Message.EqualTo("negatives not allowed: -1,-6"), 
						  new TestDelegate(() => calc.Add("-1,1,2,-6")));
		}

		[Test]
		public void ReturnSumWithIgnoringOfNumbersBiggerThatOneThousand()
		{
			var calc = new Calculator();

			var result = calc.Add("2,1001");
			
			Assert.That(2, Is.EqualTo(result));
		}

		[Test]
		public void ReturnSumIfPassedNumberWithDifferentDelimiterOfAnyLength()
		{
			var calc = new Calculator();

			var result = calc.Add("//[***]\n1***2***3");

			Assert.That(6, Is.EqualTo(result));
		}

		[Test]
		public void ReturnSumIfPassedNumberWithMultipleNumberOfDifferentDelimiters()
		{
			var calc = new Calculator();

			var result = calc.Add("//[*][%]\n1*2%3");

			Assert.That(6, Is.EqualTo(result));
		}

		[Test]
		public void ReturnSumIfPassedNumberWithMultipleNumberOfDifferentDelimitersOfAnyLength()
		{
			var calc = new Calculator();

			var result = calc.Add("//[ffg][^]\n1ffg2^1ffg1");

			Assert.That(5, Is.EqualTo(result));
		}
	}
}
